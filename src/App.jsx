import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "./components/Register";
import GameList from "./components/GameList";
import Game from "./components/Game";
import GameDetail from "./components/GameDetail";
import LandingPage from "./components/LandingPage";
import Login from "./components/Login";
import {UserProfile} from "./components/UserProfile";
import UserStory from "./components/UserStory";
import UserEdit from "./components/UserEdit";
import Playgame from "./components/Playgame";
import Badge from "./components/Badge";
import Leaderboard from "./components/Leaderboard";
import Newgame from "./components/Newgame";
import Share from "./components/Share";
import ForgotPassword from "./components/ForgotPassword"


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/forgotpassword" element={<ForgotPassword />} />
        <Route path="/room" element={<GameList />} />
        <Route path="/room/game" element={<Game />} />
        <Route path="/room/game-detail" element={<GameDetail />} />
        <Route path="/profile" element={<UserProfile />} />
        <Route path="/story" element={<UserStory/>} />
        <Route path="/user-edit/:id" element={<UserEdit />} />
        <Route path="/playgame" element={<Playgame />} />
        <Route path="/badge" element={<Badge />} />
        <Route path="/leaderboard" element={<Leaderboard />} />
        <Route path="/Newgame" element={<Newgame />} />
        <Route path="/share" element={<Share />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
