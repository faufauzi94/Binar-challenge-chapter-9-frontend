import NavigationBar from "./Navbar/NavigationBar";

import image from "../assets/image/maintenance.png";

import "../assets/styles/UserEdit.css";

const UserEdit = () => {
  return (
    <div>
      <NavigationBar />
      <div className="body-maintenance">
        <img src={image} className="imgcenter" alt="maintenance" />
        <p className="txtblue">
          Sorry, this page is still under maintenance.
          <br />
          Our developers are working on serving you a better page as soon as
          possible.
        </p>
        <p className="txtwhite">
          <strong>Thank You!</strong>
        </p>
      </div>
    </div>
  );
};

export default UserEdit;
