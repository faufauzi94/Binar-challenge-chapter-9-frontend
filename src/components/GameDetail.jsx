import { useState } from "react";
import NavigationBar from "./Navbar/NavigationBar";

function Play(props) {
  const { status } = props;

  if (status === true) {
    return (
    <>
     <a href="/playgame">  <img src={require('../assets/image/logo.png')} style={{ width: 40, height: 40, margin:10 }} alt='' /> Rock-Paper-Scissor </a>
    </>
  );

  }
  return <>Game Detail</>;
}

function Gamedetail() {
  const [playGame, setPlayGame] = useState(false);
  return (
    <>
      <NavigationBar />
      <ul>
        <li>
          <h1> Game Play</h1>
          <h1>Choose your game:</h1>
          <button onClick={() => setPlayGame(true)}>
            Play Game | Click to Play ("")
          </button>
        </li>
      </ul>
      <Play status={playGame} />
    </>
  );
}
export default Gamedetail;
