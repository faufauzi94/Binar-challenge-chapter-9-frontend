import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "../config/firebaseInit";
import { useEffect, useState, useRef } from "react";
import '../assets/styles/Leaderboard.css';
import NavigationBar from "./Navbar/NavigationBar";
import level2 from "../assets/image/level2.png";
import level5 from "../assets/image/level5.png";
import level9 from "../assets/image/level9.png";
import level10 from "../assets/image/level10.png";



const Leaderboard = () => {
    const [ user, setUser] = useState([]);
    const username = useRef('')
    useEffect(() => {
      const db = getDatabase(firebaseInit);
      const dbRef = ref(db, 'user');
      onValue(dbRef, (snapshot) => {
        setUser(snapshot.val())
  
      })
    }, []);

    const handleClick=(e)=>{
        console.log(e.target)
    }

    return (
        
        <div className="board">
            <NavigationBar />
            <h1 className='leaderboard'>Leaderboard</h1>
    
            <div className="duration">
                <button onClick={handleClick} data-id='7'>7 Days</button>
                <button onClick={handleClick} data-id='30'>30 Days</button>
                <button onClick={handleClick} data-id='0'>All-Time</button>
            </div>
            <div id="profile">
            <div class="card"> 
                <div className="flex">
                  <div className="item">
                    <div className="info">
                    
                        {user && user.map((user, key) => {
                          return (
                            <table class="table">
                            <tr key={key}>
                                <th>  {user.username}</th>
                           </tr><br/>  
                            </table>
                            );
                           })}
                    

                    </div>
                </div>
                    <div className="info">
                        {user && user.map((user, key) => {
                          return (
                            <table class="table">
                            <tr key={key}>
                                <th>  {user.score}</th>
                           </tr><br/>  
                            </table>
                         );
                        })}
                </div>
                <div className="info">
                    <table >
                        <th >
                            <tr class="level"><img src={level9} alt="" /></tr>
                            <tr class="level"><img src={level5} alt="" /></tr>
                            <tr class="level"><img src={level5} alt="" /></tr>
                            <tr class="level"><img src={level5} alt="" /></tr>
                            <tr class="level"><img src={level10} alt="" /></tr>
                            <tr class="level"><img src={level2} alt="" /></tr>
                        </th>
                    </table>
                        
                </div>
               </div>
               
            </div>
            </div>
        </div>
    )
}

export default Leaderboard;
