import React, { useState } from "react";
import "../assets/styles/Register.css"
import { auth } from '../config/firebaseInit'
import { useNavigate } from "react-router-dom";
import {createUserWithEmailAndPassword} from "firebase/auth"


const Register = () => {

  const navigate = useNavigate()

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const register = () => {
    createUserWithEmailAndPassword(auth, email, password)
    .then(auth=>{navigate("/login")})
    .catch(error=>alert(error))
  }

  return (
    <div className="">
      <div className="list">
        <div>
          <a href="/">
            <button>LandingPage</button>
          </a>
          <a href="/register">
            <button>register</button>
          </a>
          <a href="/login">
            <button>login</button>
          </a>
          <a href="/room">
            <button>game list page</button>
          </a>
          <a href="/room/game">
            <button>game</button>
          </a>
          <a href="/room/game-detail">
            <button>game detail page</button>
          </a>
          <a href="/profile">
            <button>UserProfile</button>
          </a>
          <a href="/user-edit/:id">
            <button>userEdit</button>
          </a>
          <a href="/badge">
            <button>BadgeAchievement</button>
          </a>  
        </div>
      </div>
      <div className="kotak">
      <h1 className="signup">
        Sign Up
      </h1>
      <table class="center">
        <tbody>
        <tr>
          <td>
            <input onChange={(event)=>setEmail(event.target.value)} id="email" autoComplete="off" className="custom-input" type="text" name="email" placeholder="Enter email"/>
          </td>
          </tr>
          <tr>
          <td>
            <input onChange={(event)=>setPassword(event.target.value)} id="confirm_password" autoComplete="off" className="custom-input" type="password" name="password" placeholder="Enter password"/>
          </td>
        </tr>
        <tr>
          <td>
            <input onChange={(event)=>setPassword(event.target.value)} id="password" autoComplete="off" className="custom-input" type="password" name="password" placeholder="Confirm passowrd"/>
          </td>
        </tr>
        <tr>
          <td>
            <p className="text">by createing an account you agree to conditions of use and privacy notice </p>
          </td>
          </tr>
        </tbody>
      </table>
      <button onClick={register} className="buttonreg">register</button>
      </div>

    </div>
  )
}

export default Register; 