import React from 'react'
import { useState } from 'react'
import { auth } from '../config/firebaseInit'
import { sendPasswordResetEmail } from 'firebase/auth'
import {  useNavigate } from 'react-router-dom'
import "../assets/styles/ForgotPassword.css"



const ForgotPassword = () => {

  const [email, setEmail] = useState("")

  const navigate = useNavigate()


    const forgot = () => {
      sendPasswordResetEmail(auth, email)
      .then(auth=>{
        alert("link mengganti password sudah dikirim lewat email, silahkan cek di email masuk atau di spam")
        navigate("/login")
        
    })
      .catch(error=>alert(error))
    }


    
    return(
      <div className='kotak'>
        <h2 className='forgotpassword'>Halaman Forgot Password</h2>
          <div >
            <input onChange={(event)=>setEmail(event.target.value)} autoComplete="off" className="emailforgot" type="email" name="email" placeholder='Insert Your Email'/>


          </div>
          <button onClick={forgot} className="forgot">Send to Email</button>
      </div>
    )
}

export default ForgotPassword
