import "../../assets/styles/NavigationBar.css";

export default function NavigationBar() {
  return (
    <div>
      <div className="topnav">
        <a href="/">LandingPage</a>
        <a href="/register">Register</a>
        <a href="/login">Login</a>
        <a href="/room">GameList</a>
        <a href="/room/game">Game</a>
        <a href="/room/game-detail">GameDetail</a>
        <a href="/profile">UserProfile</a>
        <a href="/story">UserStory</a>
        <a href="/user-edit/:id">UserEdit</a>
        <a href="/leaderboard">Leaderboard </a>
        <a href="/Newgame">Newgame </a>
        <a href="/share">Share </a>
      </div>
    </div>
  );
}
