import React from "react";
import { Navbar, Container, Nav } from 'react-bootstrap'
import '../../assets/styles/NavBar.css'
// import {Login} from "../login";

function NaviBar() {
    return (
        <div className="navigationpage">
          <Navbar collapseOnSelect expand="lg" variant="dark" id="navigation">
            <Container fluid>
              <Navbar.Brand href="/">LOGO</Navbar.Brand>
              <Navbar.Toggle aria-controls="responsive-navbar-nav" />
              <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="nav-left">
                  <Nav.Link href="/">HOME</Nav.Link>
                  <Nav.Link href="#">WORK</Nav.Link>
                  <Nav.Link href="#">CONTACT</Nav.Link>
                  <Nav.Link href="#">ABOUT ME</Nav.Link>
                </Nav>
                <Nav className="nav-right">
                  <Nav.Link href="/register">SIGN UP</Nav.Link>
                  <Nav.Link href="/login">LOGIN</Nav.Link>
                  <div>
                    <p>{localStorage.getItem("name")}</p>
                    <p>{localStorage.getItem("email")}</p>
                    <img src={localStorage.getItem("profilePic")} alt="profile-pic" />
                  </div>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </div>
  )};
export default NaviBar;
