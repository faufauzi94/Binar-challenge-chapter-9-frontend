import React, { useState } from "react";
import "../assets/styles/Login.css"
import { auth } from '../config/firebaseInit'
import { useNavigate } from "react-router-dom";
import {signInWithEmailAndPassword, GoogleAuthProvider, signInWithPopup, } from "firebase/auth"



const Login = () => {

  const navigate = useNavigate()


  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const signIn = () => {
    signInWithEmailAndPassword(auth, email, password)
    .then(auth=>{
      const accountName = auth.user.displayName
      const accountEmail = auth.user.email
      const accountPic = auth.user.photoURL

      localStorage.setItem("name", accountName)
      localStorage.setItem("email", accountEmail)
      localStorage.setItem("profilePic", accountPic)
    
      navigate("/room")
    })
    .catch(error=>alert(error))
  }

  const provider = new GoogleAuthProvider();

  const signInWithGoogle = () =>{
    signInWithPopup(auth, provider)
    .then((result)=>{
      const accountName = result.user.displayName
      const accountEmail = result.user.email
      const accountPic = result.user.photoURL

      localStorage.setItem("name", accountName)
      localStorage.setItem("email", accountEmail)
      localStorage.setItem("profilePic", accountPic)
    
      navigate("/room")
    })
    .catch(error => alert(error))
  }

  

  return (

    <div className="">
      <div className="list">
        <div>
          <a href="/">
            <button>LandingPage</button>
          </a>
          <a href="/register">
            <button>register</button>
          </a>
          <a href="/login">
            <button>login</button>
          </a>
          <a href="/room">
            <button>game list page</button>
          </a>
          <a href="/room/game">
            <button>game</button>
          </a>
          <a href="/room/game-detail">
            <button>game detail page</button>
          </a>
          <a href="/profile">
            <button>UserProfile</button>
          </a>
          <a href="/user-edit/:id">
            <button>userEdit</button>
          </a>
          <a href="/badge">
            <button>BadgeAchievement</button>
          </a>  
        </div>
      </div>
      <div className="kotak">
        <h1 className="signin">
          Sign in
        </h1>
        <label className="label">Email</label>
        <input onChange={(event)=>setEmail(event.target.value)} autoComplete="off" className="input" type="email" name="email"/> <br />
        <label className="label">Password</label>
        <input onChange={(event)=>setPassword(event.target.value)} autoComplete="off" className="input" type="password" name="password"/>
        <button onClick={signIn} className="button">Sign in</button>

        <p className="registerLink" onClick={()=>navigate("/forgotpassword")}>forgot password?</p>
        <p className="registerLink" onClick={()=>navigate("/register")} >don't have account? Register</p>
        <i class="fa-brands fa-google"></i>
        <button onClick={signInWithGoogle} className="buttonsignin">Sign in with Google</button>
      </div>
    </div>
  )
}

export default Login; 
