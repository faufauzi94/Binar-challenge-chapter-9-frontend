import React from "react";
import '../assets/styles/LandingPage.css'
import NaviBar from './Navbar/NavBar'
import Cards from './Card/Card'
import CarouselBox from './Card/CarouselBox'
import Footer from './Footer/Footer'


const Homepage = () => {
  return (
    <>
    <div className='home-bg'>
        <NaviBar />
        <div class="row align-items-center" id="main-bg">
            <div class="col-12 text-center">
                <h1>PLAY TRADITIONAL GAME</h1>
                <p class="my-4">Experience new traditional game</p>
                <button type="button" class="btn btn-warning btn-sm mt-2 px-5">
                    <small>
                        <a href="/login">PLAY NOW</a>
                    </small>
                </button>
            </div>
            <div class="thestory text-center">
                <p>THE STORY</p>
                <img src={require('../assets/image/scroll-down.svg').default} alt='' />
            </div>
        </div>
    </div>
   
    <div class='container' id='section2'>
        <div class='row'>
            <div class='col-md-3 offset-1'>
                <p>What's so special?</p>
                <h1>THE GAMES</h1>
            </div>
            <div class='col-md-7'>
                <CarouselBox />
            </div>
        </div>
    </div>

    <div class='container' id='section3'>
        <div class='row'>
            <div class='col-sm-4 offset-7'>
                <p class='top-p'>What's so special?</p>
                <h1>FEATURES</h1>
                <ul class='list-features'>
                    <li class='detail-list'>
                        <h5>TRADITIONAL GAME</h5>
                        <p>If you miss your childhood, we provide many traditional games here</p>
                    </li>
                    <li>GAME SUIT</li>
                    <li>TBD</li>
                </ul>
            </div>
        </div>
    </div>

    <div class='container' id='section4'>
        <div class='row'>
            <p class='text-center'>Can My Computer Run This Game?</p>
            <div class="col-sm-5 offset-1">
                <h1>SYSTEM REQUIREMENT</h1>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <p class="text-orange">OS:</p>
                            <p>Windows 7 64-bit only<br/>(No OSX support at this time)</p>
                        </td>
                        <td>
                            <p class="text-orange">PROCESSOR:</p>
                            <p>Intel Core 2 Duo @ 2.4 GHZ or AMD Athlon X2 @ 2.8 GHZ </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="text-orange">MEMORY:</p>
                            <p> 4 GB RAM </p>
                        </td>
                        <td>
                            <p class="text-orange">STORAGE:</p>
                            <p> 8 GB available space </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p class="text-orange">GRAPHICS:</p>
                            <p> NVIDIA GeForce GTX 660 2GB or<br/>AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class='container' id='section5'>
        <div class='row'>
            <div class='col-sm-4 offset-1'>
                <h1>TOP SCORES</h1>
                <p>This top score from various games provided on this platform</p>
                <button type='button' class='btn btn-md px-5 btn-warning'>see more</button>
            </div>
            <div class='col-sm-5 offset-1'>
                <Cards />
            </div>
        </div>
    </div>

    <div class='container' id='section6'>
        <div class='row'>
            <div class='col-md-3 offset-2'>
                <img src={require('../assets/image/skull.png')} alt='' />
            </div>
            <div class='col-md-6 offset-1'>
                <p class='top-text'>Want to stay in<br/>touch?</p>
                <h1>NEWSLETTER SUBSCRIBE</h1>
                <p class='desc'>In order to start receiving our news, all you have to do is enter your email address. Everything else will be taken care of by us. We will send you emails containing information about game. We don't spam.</p>
                <div class="row justify-content-start text-white fw-bold">
                    <form>
                        <div class="row g-3 align-items-center">
                            <div class="col-auto">
                                <input type="email" placeholder="Your email address" class="form-control px-5 py-2" id="exampleInputEmail1" aria-describedby="emailHelp" />
                            </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-warning px-4 py-2 btn-sm">Subscribe now</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <Footer />
</>
)
}

export default Homepage;
