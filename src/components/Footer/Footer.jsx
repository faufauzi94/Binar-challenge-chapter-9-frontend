import React from "react";
import '../../assets/styles/Footer.css'


function Footer() {
    return (
        <div className="container" id="footer">
            <div class="col-lg-10 offset-1 mt-0">
                <div class="row footer-menu justify-content-end">
                    <div class="col-auto footer-menu-item">MAIN</div>
                    <div class="col-auto footer-menu-item">ABOUT</div>
                    <div class="col-auto footer-menu-item">GAME FEATURES</div>
                    <div class="col-auto footer-menu-item">SYSTEM REQUIREMENTS</div>
                    <div class="col-auto footer-menu-item">QUOTES</div>
                    <div class="col-auto footer-menu-item">
                        <img src={require('../../assets/image/facebook.svg').default} alt="" />
                    </div>
                    <div class="col-auto footer-menu-item">
                        <img src={require('../../assets/image/twitter.svg').default} alt="" />
                    </div>
                    <div class="col-auto footer-menu-item">
                        <img src={require('../../assets/image/vector.svg').default} alt="" />
                    </div>
                    <div class="col-auto footer-menu-item">
                        <img src={require('../../assets/image/twitch.svg').default} alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-10 offset-1 mt-0">
                <hr />
                <div class="row justify-content-between">
                    <div class="col-6 copyright">
                        <p class="text-start">© 2010 Your Games, Inc. All Rights Reserved</p>
                    </div>
                    <div class="col-6 privacy">
                        <p class="text-end">PRIVACY POLICY | TERMS OF SERVICES | CODE OF CONDUCT</p>
                    </div>
                </div> 
            </div> 
        </div>
    )
}

export default Footer;
