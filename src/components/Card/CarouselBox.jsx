import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import '../../assets/styles/CarouselBox.css'

function CarouselBox() {
  return (
    <Carousel>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src={require('../../assets/image/rockpaperstrategy-1600.jpg')}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={800}>
        <img
          className="d-block w-100"
          src={require('../../assets/image/rockpaperstrategy-1600.jpg')}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={800}>
        <img
          className="d-block w-100"
          src={require('../../assets/image/rockpaperstrategy-1600.jpg')}
          alt="Third slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={800}>
        <img
          className="d-block w-100"
          src={require('../../assets/image/rockpaperstrategy-1600.jpg')}
          alt="Fourth slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../../assets/image/rockpaperstrategy-1600.jpg')}
          alt="Fifth slide"
        />
      </Carousel.Item>
    </Carousel>
  );
}

export default CarouselBox;
