import React from "react";

import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LineIcon,
  LineShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "react-share";

import NavigationBar from "./Navbar/NavigationBar";

export default function Share() {
  const url =
    "https://gitlab.com/faufauzi94/Binar-challenge-chapter-9-frontend/-/tree/main/";

  const title = "Binar Academy Challenge Chapter 9";

  return (
    <div>
      <NavigationBar />
      <h3 className="text-center mt-4">Share to:</h3>
      <div className="mt-2 text-center">
        <FacebookShareButton url={url} quote={title}>
          <FacebookIcon size={50} />
        </FacebookShareButton>

        <EmailShareButton url={url} quote={title}>
          <EmailIcon size={50} />
        </EmailShareButton>

        <LineShareButton url={url} quote={title}>
          <LineIcon size={50} />
        </LineShareButton>

        <LinkedinShareButton url={url} quote={title}>
          <LinkedinIcon size={50} />
        </LinkedinShareButton>

        <TelegramShareButton url={url} quote={title}>
          <TelegramIcon size={50} />
        </TelegramShareButton>

        <TwitterShareButton url={url} quote={title}>
          <TwitterIcon size={50} />
        </TwitterShareButton>

        <WhatsappShareButton url={url} quote={title}>
          <WhatsappIcon size={50} />
        </WhatsappShareButton>
      </div>
    </div>
  );
}
