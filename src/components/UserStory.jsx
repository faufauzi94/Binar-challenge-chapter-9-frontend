import React, { Component } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import firebaseInit from "../config/firebaseInit";

import "bootstrap/dist/css/bootstrap.min.css";
import "../assets/styles/UserProfileTable.css";

import NavigationBar from "./Navbar/NavigationBar";

const db = getDatabase(firebaseInit);

export class UserStory extends Component {
  constructor() {
    super();
    this.state = {
      userData: [],
    };
  }

  componentDidMount() {
    const dbRef = ref(db, "userstory");
    onValue(dbRef, (snapshot) => {
      let record = [];
      snapshot.forEach((childSnapshot) => {
        let keyName = childSnapshot.key;
        let data = childSnapshot.val();
        record.push({ key: keyName, data: data });
      });

      this.setState({ userData: record });
    });
  }

  render() {
    return (
      
      <div>
        <NavigationBar />
        <h3 className="text-center mt-4">UserStory</h3>
        <div class="table-wrapper">
          <table class="fl-table">
            <thead>
              <tr>
                <th>#</th>
                <th>username</th>
                <th>Win</th>
                <th>Draw</th>
                <th>Lose</th>
              </tr>
            </thead>
            <tbody>
              {this.state.userData.map((row, index) => {
                return (
                  <>
                    <tr>
                      <td>{index}</td>
                      <td>{row.data.username}</td>
                      <td>{row.data.win}</td>
                      <td>{row.data.draw}</td>
                      <td>{row.data.lose}</td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
      
    );
    
  }
}
export default UserStory;
