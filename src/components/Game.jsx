import NavigationBar from "./Navbar/NavigationBar";

const Game = () => {
  return (
    <div>
      <NavigationBar />
      <div className="text-center mt-4">
        <h1>Play Traditional Game</h1>
        <h4>
          Experience new traditional game. Just play it and having fun with it..
        </h4>
        <br />
        <h5>Made by: Binar FSW 25 Team 1</h5>
        <h5>Made for: Binar Challenge 9</h5> <br />
        <br />
        <br />
        <br />
        <br />
        <h5>Team Role:</h5> <br />
        <h5> # Scrum Master : Sylvia Listyani</h5>
        <h5> # Repo Builder & Maintenance : Fauzi Fauzi</h5>
        <h5> # Repo Maintenance : Mohammad Fauzan Z</h5>
        <h5> # Boilerplate Coder : Muhammad Akmal F</h5>
        <h5> # Devops : Andri Anggoro</h5>
      </div>
    </div>
  );
};

export default Game;
