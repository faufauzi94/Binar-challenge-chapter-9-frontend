import NavigationBar from "./Navbar/NavigationBar";
import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

import image1 from "../assets/image/game1.jpeg";
import image2 from "../assets/image/logo.png";
import image3 from "../assets/image/checkers.jpg";

export default function GameList() {
  return (
    <div className="text-center">
      <NavigationBar />
      <h1 className="text-center mt-3">GameList</h1>
      <div
        className="d-flex justify-content-around"
        style={{ marginTop: "25px" }}
      >
        {/* Game1 */}
        <Card style={{ width: "18rem" }}>
          <Card.Img variant="top" src={image1} />
          <Card.Body>
            <Card.Title>Fireboy and Lavagirl in The Forest Temple</Card.Title>
            <Button
              variant="primary"
              onClick={() => {
                alert("Game is not available!");
              }}
            >
              Details
            </Button>
          </Card.Body>
        </Card>

        {/* Game2 */}
        <Card style={{ width: "18rem" }}>
          <Card.Img variant="top" src={image2} />
          <Card.Body>
            <Card.Title>Rock Paper Scissors Game</Card.Title>
            <Button
              variant="primary"
              style={{ marginTop: "20px" }}
              href="/room/game-detail"
            >
              Details
            </Button>
          </Card.Body>
        </Card>

        {/* Game3 */}
        <Card style={{ width: "18rem" }}>
          <Card.Img variant="top" src={image3} />
          <Card.Body>
            <Card.Title>Casual Checkers</Card.Title>
            <Button
              variant="primary"
              style={{ marginTop: "20px" }}
              onClick={() => {
                alert("Game is not available!");
              }}
            >
              Details
            </Button>
          </Card.Body>
        </Card>
      </div>

      <div className="text-center mb-4">
        <Button variant="danger" href="/badge" style={{ marginTop: "25px" }}>
          Badge Achievement
        </Button>
      </div>
      {/* <a href="/badge">
        <button>BadgeAchievement</button>
      </a> */}
    </div>
  );
}
